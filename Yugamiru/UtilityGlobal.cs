﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    //Class added by sumit on 25-Feb-2019 for general purpose use
    public static class UtilityGlobal
    {
        public static clsIDNameHandler ID_NAME_Handler;
        public static void Set_ID_NAME_Handler(string fullID, string fullName, string AppLanguage)
        {
            ID_NAME_Handler = new clsIDNameHandler(fullID,fullName, AppLanguage);            
        }
    }

    
}
